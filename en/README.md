---
Description: this page is the english home page of the project
---

# The 10 specifications of a Free Hardware, in an ideal, absolute, canonic sense of the term

You are on the english home page of this "The 10 specifications of a Free Hardware, in an ideal, absolute, canonic sense of the term" project on gitlab.com.

The native language of the project is french.

English is an official language for the project.

But the project has not been written in english language yet.

We will pleased to wellcome people who would like to help to write the english side of the project.

If you wish to contribute, please contact us: sign-in to `gitlab.com` , [go the home page of the project](https://gitlab.com/ant-c-collectif/materiel-libre/concept-definitions-materiel-libre/10-caracteristiques-ideales-materiel-libre), ask to be part of the team, and post an issue.