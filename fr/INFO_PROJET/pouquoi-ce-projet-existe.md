---
Description: explique pourquoi ce projet "les 10 cararactéristiques d'un matériel libre idéal absolu" existe
---

# Pourquoi ce projet existe-il ?

*Note:* cette page vous donne des informations sur les raisons pour lesquelles ce projet existe. En plus du contenu de cette page, vous pouvez aussi consulter [le message de la personne qui est à l'initiative du projet ](/fr/INFO_PROJET/message-personne-porteuse-du-projet.md) dans lequel vous trouverez d'autres informations sur les raisons d'existence de ce projet.


## Motivations

Ce projet existe, parce que:

*  au moment où ce projet voit le jour, il n'existe pas de définition satisfaisante pour exprimer ce que devrait être un matériel libre au sens idéal, absolu, canonique du terme

Tenter de palier à ce manque, c'est ce qui a motivé ce projet, c'est ce qui a présidé à la création de ce projet.

*Note:* Si vous pensez que la définition d'un matériel libre existe déjà, que tout a été dit et que ce projet n'a pas de raison d'être, vous devriez [consulter où en est la communauté internationale experte et légitimée quant à la définition du matériel libre. ](/fr/INFO_PROJET/etat-des-lieux-notion-materiel-libre.md)


## Intentions

L'intention de ce projet consiste à:

*  essayer de produire, de façon collaborative, un texte décrivant ce que pourrait être la notion de matériel libre, au sens idéal, absolu, canonique du terme, permettant de reproduire les effets des principes qui régissent la définition d'un logiciel libre, pour les fabrications de choses matérielles.
  
*Note:* Si vous pensez que la définition d'un matériel libre existe déjà, que tout a été dit et que ce projet n'a pas de raison d'être, vous devriez [consulter où en est la communauté internationale experte et légitimée quant à la définition du matériel libre. ](/fr/INFO_PROJET/etat-des-lieux-notion-materiel-libre.md)


## Buts

*  Délivrer un texte susceptible de délivrer une définition étalon de ce qu'est un matériel libre au sens idéal, absolu, canonique du terme. 
*  Trouver ensuite des moyens pour caractériser la distance qui sépare une chose matérielle, de cette notion de matériel libre prise au sens idéal, absolu, canonique du terme. 
*  Délivrer un texte étalon de référence qui puisse servir lors de la rédaction de "termes et de conditions" (des licences ou équivalent) spécifiques aux fabrications matérielles désireuses d'être réalisées et mises à disposition sous "licence libre" (droits free-libre-open-source)