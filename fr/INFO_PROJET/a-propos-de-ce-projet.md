---
description: "Donne des informations à propos du projet de texte les 10 caractéristiques d'un matériel libre idéal
---

[Pour d'autres informations, consultez la page d'accueil du projet](/fr/README.md)


# Ce projet: de quoi s'agit-il ?

*  **Vocation:** Ce projet a pour vocation d'essayer de produire un texte susceptible de décrire correctement les caractéristiques que devrait avoir un matériel libre au sens idéal, absolu, canonique du terme. 
*  **Type de projet:** Ce projet est un projet expérimental de recherche appliquée libre et indépendante relatif à la notion de matériel libre. 
*  **Lien avec d'autres projets:** Ce projet est l'une des parties d'[un projet expérimental plus vaste](/fr/INFO_PROJET/appartenance-a-un-projet-plus-vaste.md).
*  **Chose produite par ce projet:** Ce projet délivre un [texte intitulé  _"les 10 caractéristiques d'un matériel libre"_](/fr/INFO_PROJET/texte-sous-differents-formats-disponibles.md) . 
*  **Utilisation:** Le texte produit par ce projet peut servir de contenu de base pour présenter la notion de matériel libre, et pour produire des licences ou des labels relatifs au matériel libre. Ce texte est confectionné de façon collaborative.
*  **Pour qui ?:** Le texte "Les 10 caractéristiques d'un matériel libre idéal absolu" produit par ce projet, est destiné à toute personne qui s'intéresse à la notion de matériel libre, ou qui aimerait avoir un éclairage sur cette notion.
*  **Quels types de travaux sont réalisés:** La rédaction de textes visant à décriore le plus correctement possible les caractéristiques que devrait avoir un matériel libre au sens idéal, absolu, canonique du terme. 
*  **Ce qui se fait:** Des discussions à distance via messages électroniques, et la rédaction de textes via gitlab
*  **Que fait ce projet:** Il fournit des éléments de réflexions sur la notion de matériel libre, et un texte expérimental, refondateur de cette notion
*  **Contexte d'utilisation:** Le texte "Les 10 caractéristiques d'un matériel libre idéal absolu" produit par ce projet, est expérimental. Il peut être utilisé dans un contexte de expérimentalou de réflexion sur le matériel libre.
*  **Exigences préalables d'utilisation:** Il est conseillé de spécifier que ce texte est un projet expérimental, et qu'il est susceptible d'évoluer.
*  **Que pouvez-vous faire avec:** Vous pouvez utiliser le texte "Les 10 caractéristiques d'un matériel libre idéal absolu" produit par ce projet, par exemple pour :
   *   discuter de la notion de matériel libre
   *   faire découvrir à quelqu'un une façon de présenter la notion de matériel libre
   *   alimenter une réflexion sur la notion de matériel libre
   *   rédiger des textes de termes et conditions régissant la notion de matériel libre (licences de matériel libre)
*  **Quels sont les avantages de ce projet par rapport à d'autres alternatives ?:**  La notion de matériel libre est souvent réduite à un matériel dont la documentation est publiée sous une licence libre adaptée à cette documentation. Le projet "Les 10 caractéristiques d'un matériel libre idéal absolu" va plus loin. En explorant et en couvrant d'autres champs relatifs aux différents aspects du matériel, le projet ouvre des perspectives supplémentaires, et pousse au maximum du possible ce que devrait être un matériel libre, bien au delà de la simple publication de sa documentation sous licence libre.

