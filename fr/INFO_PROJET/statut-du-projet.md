---
Description: cette page délivre des informations sur l'état du projet, son statut
---

# Statut du projet

*  actif
*  rythme d'avancée lent
*  peu de personnes participent
*  phase de versionnage et de traduction des premières versions