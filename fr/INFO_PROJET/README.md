---
description: "ce document donne des liens principaux pour en savoir plus sur ce projet"
---

_Fichier README.md_

Statut de ce document: en cours d'écriture `[=>---------][10%]` (avril 2020)




# Projet : "Les 10 caractéristiques d'un matériel libre idéal absolu"

| Travaux en cours |

| Wiki | Doc | Versions |

## De quoi s'agit-il ?

*  [Informations générales sur ce projet](/fr/INFO_PROJET/a-propos-de-ce-projet.md)
*  [Texte "les 10 caratéristiques d'un matériel libre" : formats disponibles ](/fr/INFO_PROJET/texte-sous-differents-formats-disponibles.md)

## Motivations, intentions, buts du projet

*  [Pourquoi ce projet existe](/fr/INFO_PROJET/pouquoi-ce-projet-existe.md)
  
## Historique

*  [Dates principales du projet](/fr/INFO_PROJET/historique.md)
*  [Historique des dépôts](/fr/INFO_PROJET/historique.md)

## Liens avec d'autres projets

*  [liens avec un projet plus vaste](/fr/INFO_PROJET/appartenance-a-un-projet-plus-vaste.md)

## Statut du projet

*  [Informations sur le statut du projet](/fr/INFO_PROJET/statut-du-projet.md)

## Savoir-faire requis pour participer

*  []

## Copie écran

![](documentation/img_copie_ecran_10_car_mat_lib.png)

## Connaissances et techniques utilisées

*  Fait avec libre office
*  Transformé en autres formats via pandoc
*  Fichiers markdown pour la documentation

## Caractéristiques

*  Particularités (lien)

## Exemples

 **how** your project solves their problem by looking at the code example. 

## Installation

Provide step by step series of examples and explanations about how to get a development env running.

## Documentation

## Publications

## Tests
Describe and show how to run the tests with code examples.

## How to use?
If people like your project they’ll want to learn how they can use it. To do so include step by step guide to use your project.

## Contribute

Let people know how they can contribute into your project. Un lien vers un contributing. (exemple d'un contributing: https://github.com/zulip/zulip-electron/blob/master/CONTRIBUTING.md)
*  Comment contribuer
*  Choses à faire actuellement (travail en cours)(aide demandée)


## Credits
 

## Remerciements


## Autres choses utiles


## Droits et conditions d'utilisation
A short snippet describing the license (MIT, Apache etc)

MIT © [Yourname]()


---

TEXTE CI-DESSOUS, ANCIENNE VERSION À RECYLCER, À REPOSITIONNER AILLEURS

## Texte "Les 10 caractéristiques d'un matériel libre idéal"

_Fichier README.md_

### Texte sous différents formats de fichiers

Le texte "Les 10 caractéristiques d'un matériel libre idéal" est disponible en plusieurs formats de fichiers:

*  fichier `.odt`
*  autres formats: `.html`, `.md`, `.pdf`, ...

Le fichier `.odt` est le fichier source.

Les autres formats ont été réalisés avec le logiciel pandoc à partir du fichier source .odt

### Localisation du texte

Le fichier source .odt se trouve dans le répertoire courant:
*  le répertoire `fr`

Les autres formats se trouvent dans le répertoire dédié:
*  le répertoire `texte-sous-autres-formats-de-fichiers`
 
### Explications sur ce texte

Des explications sur ce texte (statuts, travaux actuels, etc ...) se trouvent dans le fichier `explications.md`

### Autres langues

Le texte est (ou sera) disponible dans d'autres langues.
*  se rendre à la racine du dépôt
*  

Source d'inspiration pour écrire ce document (à retirer une fois le doc terminé)
*  [A Beginners Guide to writing a Kickass README ✍](https://medium.com/@meakaakka/a-beginners-guide-to-writing-a-kickass-readme-7ac01da88ab3)