---
Description: donne des informations sur le projet qui englobe ce projet des "10 caractéristiques d'un matériel libre idéal"
---

# Lien avec d'autres projets

## Appartenance à un projet plus grand

Ce projet fait partie d'un projet expériemntal global, portant sur la notion de matériel libre, incluant aussi bien 

*  des réflexions sur la notion de matériel libre (lien à fournir FIXME)
*  des expérimentations de mise en pratique, au travaers d'essais de conceptions et de fabrications de matériel libre (lien à fournir FIXME)
*  des réflexions sur les aspects stratégiques de projets de matériel libre (lien à fournir FIXME)
*  des expérimentations de rédactions de termes et conditions ("licences") pouvant encadrer des projets de matériel libre (lien à fournir FIXME)
*  des réflexions et des expérimentations de pratiques collaboratives à distance et en présentiel, pour produire l'ensemble de ces contenus et réalisations (lien à fournir FIXME)  

Voir le [dépôt originel du projet "matériel-libre"](https://framagit.org/materiel-libre) dont ce projet fait partie.