---
Description: cette page donne des informations sur l'état des lieux de la notion de Matériel Libre au moment où débute le projet
---

# Vous avez-dit "Matériel Libre" ?

## La notion Matériel Libre : plusieurs appellations et des variations dans l'interprétation

Le terme "Matériel Libre" porte plusieurs appellations:

*  Open Hardware: c'est le terme choisi par les licences [TAPR-OHL](https://tapr.org/the-tapr-open-hardware-license/), [CERN-OHL](https://ohwr.org/project/cernohl/wikis/home), et [SOLDERPAD OHL](https://solderpad.org/licenses/)
*  Opensource Hardware: c'est le terme choisi par [freedomdefined](https://freedomdefined.org/OSHW#Endorsements) et par [OSHWA](https://www.oshwa.org/definition/) 
*  Matériel Libre: c'est le [terme français qui a est communément utilisé pour signifier aussi bien Free Hardware, Open Hardware, ou Open-source Hardware](https://fr.wikipedia.org/wiki/Mat%C3%A9riel_libre)
*  Matériel Libre car sa documentation est sous licence libre: c'est [ce que dit la FSF à propos du terme Free Harware (Matéreil Libre)](https://www.gnu.org/philosophy/free-hardware-designs.fr.html)

## La communauté internationale ne reconnaît pas la notion de Matériel Libre stricto-sensus

Au moment où débute ce projet, la notion de matériel libre n'a pas été retenue comme une notion valide par les personnes légitimées pour décider de ce type de sujet.

Selon certaines personnes, l’équivalent de la notion « logiciel libre » applicable aux choses notion de « matériel libre » ne peut exister.

À l'heure où a débuté ce projet, de nombreuses personnes impliquées dans les notions dites FL-OS [1] (aussi bien des personnes expertes en rédactions de termes et conditions générant des licences libres open source, que des personnes praticiennes de choses publiées sous licence libre open source, personnes humaines et personnes morales) de part le monde, ont participé aux réflexions sur la notion de matériel libre. 

Toutes ces personnes se sont accordées à dire qu’il n’est pas possible de parler d’une notion de matériel libre qui serait à l’équivalent de ce qu’est la notion de logiciel libre appliquée aux choses matérielles. En cause : la matière, qui empêche de récréer l’équivalent des libertés portée par la notion FL-OS, pour des raisons de chaîne approvisionnements (incluant le poids du coût et le poids logistique), pour des raisons d’absence de cadre légale pour créer des conditions identiques à celles des œuvres de l’esprit, et pour des raisons liées aux brevets (les brevets agissent sur la fabrication des choses matérielles) 

Tout au moins, ces personnes tolèrent trois postures relatives à la notion de « matériel libre ».

*  **Posture 1/** au lieu de parler de Matériel Libre, il serait plus approprié de parler d’Open-Hardware (matériel ouvert) ou OpenSource Hardware (matériel opensource). Et sous cette appellation, de déterminer ce qui peut relever d’une telle catégorie et ce qui ne peut pas. 

*  **Posture 2/** on peut éventuellement parler de Matériel Libre, comme une contraction du terme "matériel fabriqué à partir de documentations libres" lorsque la documentation complète de ce matériel est délivré sous une licence libre au sens donné par la Free Software Foundation (FSF), incluant le fait qu’aucun contenu de cette documentation ne puisse être entravé par un brevet pour la fabrication du matériel en question.

*  **Posture 3/** quelques rares licences (TAPR_OHL, CERN-OHL, SOLDERPAD_OHL) développées spécifiquement pour la fabrication de choses matérielles, portent le qualificatif OHL pour Open-Hardware-Licence, reproduisent les conditions FL-OS d’une part pour la documentation de choses à fabriquer et d’autre part pour l’utilisation de cette documentation pour leurs fabrications.

## Matériel Libre : une traduction française fourre-tout

En français, c'est le terme _Matériel Libre_ qui a été retenu pour traduire tout aussi bien:

*  le terme "free hardware"
*  le terme "openhardware"
*  le terme "opensource hardware"
  
C'est un peu un terme fourre-tout.

## Matériel Libre : une illusion ?

Si l'on transpose directement le concept de logiciel libre au matériel, matériel libre signifierait alors que l'utilisateur est libre d'utiliser ce matériel, de le copier et de le redistribuer, avec ou sans modification. 

Or, cela pose question. Copier une chose matérielle peut être compliqué. Dupliquer sa fabrication aussi. Obtenir les composants et être certain que ces derniers sont aussi des matériels libres, avoir la certitude que les fonctions et les designs sont sous droit ouverts, ne pas publier sous licence libre des composants ou des fonctionnalités qui sont dans le domaine public, ce n'est pas si simple. 

C'est la raison pour laquelle le terme matériel libre est susceptible de générer une perception erronnée, une illusion.

## Vers une définition d'un matériel libre, au sens idéal, absolu,canonique du terme

Pour éviter que le terme matériel libre induise en erreur, génère des perceptions erronnées, soit un terme fourre-tout, il paraît nécessaire de tenter de définir ce que pourrait être la notion de matériel libre, au sens idéal, absolu, canonique du terme. C'est l'objet de ce projet.

## Notes de bas de page

[1] FL-OS pour Free Libre Open Source . Par déclinaison FLOSS signifie Free Libre Open Source Software (pour les logiciels), FLOSC pour Free Libre Open-Source Culture (pour les choses culturelles), FLOSH pour Free Libre Open-Source Hardware, FL-OS-HL Pour Free Libre Open-Source Hardware Licence.

