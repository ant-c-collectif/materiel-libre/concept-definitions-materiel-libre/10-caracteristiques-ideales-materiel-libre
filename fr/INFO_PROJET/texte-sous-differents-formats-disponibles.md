---
Description: liste les différents formats de fichiers dans lesquels le texte "les 10 caractéristiques d'un matériel libre idéal absolu" est disponible
---

# Le texte "les 10 caractéristiques d'un matériel libre idéal absolu" est disponible sous différents formats de fichier.

Le processus de rédaciton est le suivant:
1.  le texte est d'abord rédigé sous Libre-Office (fichier source). 
2.  Puis il est converti dans d'autres formats en utilisant le logiciel Pandoc.

## Fichier source

Le texte "les 10 caractéristiques d'un matériel libre idéal absolu" est d'abord rédigé sous format libre-office ODT :
*  Texte source au format [ODT](/fr/TEXTE_SOURCE_AU_FORMAT_LIBRE_OFFICE/odt-LES-10-CARACTERISTIQUES-D-UN-MATERIEL-LIBRE-AU-SENS-IDEAL-ABSOLU-CANONIQUE-DU-TERME.odt) (`Fichier Source`)

## Conversion dans d'autres formats

Après avoir été rédigé sous Libre-Office, le texte "les 10 caractéristiques d'un matériel libre idéal absolu" est converti - en utilisant le logiciel `pandoc` - dans les autres formats ci-dessous: 
*  Texte converti au format [PDF](/fr/TEXTE_CONVERTI_EN_AUTRES_FORMATS/pdf-LES-10-CARACTERISTIQUES-D-UN-MATERIEL-LIBRE-AU-SENS-IDEAL-ABSOLU-CANONIQUE-DU-TERME.pdf) 
*  Texte converti au format [HTML](/fr/TEXTE_CONVERTI_EN_AUTRES_FORMATS/html-LES-10-CARACTERISTIQUES-D-UN-MATERIEL-LIBRE-AU-SENS-IDEAL-ABSOLU-CANONIQUE-DU-TERME.html) 
*  Texte converti au format [MARKDOWN](/fr/TEXTE_CONVERTI_EN_AUTRES_FORMATS/md-LES-10-CARACTERISTIQUES-D-UN-MATERIEL-LIBRE-AU-SENS-IDEAL-ABSOLU-CANONIQUE-DU-TERME.md) 
*  texte converti au format [TEXTE](/fr/TEXTE_CONVERTI_EN_AUTRES_FORMATS/txt-LES-10-CARACTERISTIQUES-D-UN-MATERIEL-LIBRE-AU-SENS-IDEAL-ABSOLU-CANONIQUE-DU-TERME.txt)