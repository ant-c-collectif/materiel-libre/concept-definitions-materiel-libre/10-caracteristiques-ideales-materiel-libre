---
Description: donne des informations sur l'historique de ce projet
---

Voir aussi: 

# Historique du projet

En plus du contenu de cette page, consultez aussi:
*  [Le message de la personne qui est à l'origine du projet](/fr/INFO_PROJET/message-personne-porteuse-du-projet.md)
*  [pourquoi ce projet existe](/fr/documentation/pourquoi_ce_projet_existe.md)

## Dates principales

*  avril-mai 2020 : améliorations du contenu du dépôt gitlab.com pour qu'il soit mieux compréhensible
*  mars 2020 - transfert du dépôt framagit vers gitlab.com
*  juillet 2019 - ouverture d'un dépôt spécifique sur framagit, et portage du contenu depuis le dépôt général du projet global vers ce dépôt spécifique
*  été 2018 - publications du texte sur lewiki labomedia
*  2016 - début des échanges pour produire ce texte
  

## Historique des dépôts

*  [Dépôt actuel sur gitlab.com](https://gitlab.com/ant-c-collectif/materiel-libre/concept-definitions-materiel-libre/10-caracteristiques-ideales-materiel-libre)
*  [Ancien dépôt spécifique, sur Framagit](https://framagit.org/materiel-libre/materiel-libre-textes-de-reference)
*  [Groupe "materiel-libre" sur Framagit](https://framagit.org/materiel-libre)
*  [Dépôt originel "materiel-libre" sur framagit](https://framagit.org/materiel-libre/materiel-libre)