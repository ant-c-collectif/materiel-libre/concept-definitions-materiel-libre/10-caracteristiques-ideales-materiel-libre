---
Description: Délivre un message de la part de la personne porteuse du projet
---

# Message de la personne qui est à l'initiative de ce projet, et qui porte ce projet

"Bonjour,

En 2003, j'ai commencé à envisager d'expérimenter le portage des pratiques utilisées dans le développement des logiciels libres, dans l'univers des choses fabriquées (les choses faites avec des atomes, ce que j'ai intitulé en anglais"Things made with atoms",TMAs). 

J'ai essayé d'imaginer une chose technique, petite, relativement simple à premier abord, pouvant embarquer des éléments techniques, scientifiques, mettant en jeu divers domaines de technicités et de technologie, et si possible, pouvant concerner un grand nombre de personnes. Et j'ai fini par choisir la fabrication d'une petite éolienne. 

Dans la foulée, je me suis tout de suite penché sur les conditions juridiques qui pouvaient s'appliquer pour faire de ce projet un projet de Matériel Libre. 

J'ai pris contact avec les personnes en charge des grandes licences de logiciel libre renommées. Nous avons échangé. J'ai ensuite cherché à transformer ces licences, les unes après les autres, pour qu'elles puissent s'adapter aux conceptions, aux fabrications et à la distribution de choses matérielles. Puis, j'ai tenté de rédiger des termes et conditions spécifiques, inspirées des différentes portions des licences de logiciels libres ou de documentation libre ou d'art libre. En parallèle de premiers travaux de conceptions et de réalisations de petites éoliennes, je continuais ces travaux de rédactions de termes et conditions. 

Or, il s'est fait jour, années après années, qu'il manquait une description de ce que devrait être un matériel libre, au sens idéal, absolu, canonique du terme. Une descritpion qui ferait office d'étalon. Et qui permettrait ensuite de pouvoir spécifier si tel ou tel matériel est plus ou moins éloigné de cette définition étalon. Et par ricochet, de pouvoir rédiger des termes et conditions spécifiques au matériel libre, donnant des indications sur la distance qui sépare un matériel de la notion étalon d'un matériel libre. 

J’ai proposé aux personnes autours de moi de participer à cette tentative de définition de ce que devrait être un matériel libre au sens idéal, absolu, canonique du terme. 

B01, Gaziel, Max, et d’autres personnes (notamment des personnes qui croisent l'univers de l'association [Labomedia](https://labomedia.org/) ont participé à ces travaux au travers d’une instance framagit. De fil en aiguille un texte a pris forme, et une première publication a été réalisée en 2017, puis des améliorations en 2018 puis une première version en 2019. 

Ce texte étant maintenant produit, il constitue une première base solide pour construire des licences et identifier si elles permettent de garantir complètement ou partiellement une notion de matériel libre. C’est ce qu’il va falloir faire maintenant …. "


_Antoine_ 