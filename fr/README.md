---
description: cette page est la page d'accueil en français de ce projet "les 10 caratéristiques d'un matériel libre au sens idéal, absolu, canonique du terme"
---

# Les 10 caratéristiques d'un matériel libre au sens idéal, absolu, canonique du terme

<!--
## Pour en savoir plus sur ce projet

*  Consultez les [informations concernant ce projet](/fr/INFO_PROJET/README.md)
-->

## Lien direct vers le contenu principal de ce projet

*  Lien vers [le texte "Les 10 caratéristiques d'un matériel libre au sens idéal, absolu, canonique du terme"mis à disposition sous différents formats](/fr/INFO_PROJET/texte-sous-differents-formats-disponibles.md)
  
