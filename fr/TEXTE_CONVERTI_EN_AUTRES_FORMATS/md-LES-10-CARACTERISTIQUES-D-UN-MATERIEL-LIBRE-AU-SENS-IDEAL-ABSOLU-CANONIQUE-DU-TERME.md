![Licence Creative
Commons](https://i.creativecommons.org/l/by-nd/4.0/88x31.png) Licence
Creative Commons BY-ND 4.0 Intl  

Ce document et son contenu, sont une œuvre de l'esprit. Cette œuvre de
l'esprit est mise à disposition par ses auteurs selon les termes de la
Licence Creative Commons Attribution - Pas de Modification 4.0
International - voir la section intitulée "Termes et Conditions de
publication de ce document et de son contenu", située en fin de
document.

------------------------------------------------------------------------

LES 10 CARACTÉRISTIQUES D’UN  MATÉRIEL LIBRE AU SENS IDÉAL ABSOLU CANONIQUE DU TERME  
======================================================================================

Version MATLIB-10CAR-19.01 

~o~

RÉSUMÉ
------

-   (01) C’EST UNE CHOSE MATÉRIELLE FABRIQUÉE (EXTRAITE, TRANSFORMÉE,
    ASSEMBLÉE, PRODUITE)
-   (02) QUE L'ON PEUT FABRIQUER AVEC DES TECHNIQUES USUELLES ET
    ACCESSIBLES UNIVERSELLEMENT  
-   (03) QUI EST DÉLIVRÉE AVEC UNE DOCUMENTATION PERMETTANT DE TOUT
    SAVOIR SUR CETTE CHOSE FABRIQUÉE  
-   (04) DONT LA DOCUMENTATION DOIT ÊTRE LIBRE  
-   (05) DONT LES LOGICIELS ET LEURS DOCUMENTATIONS DOIVENT ÊTRE
    LIBRES  
-   (06) DONT LES MODIFICATIONS FONT L’OBJET D’UNE DOCUMENTATION LIBRE  
-   (07) DONT TOUS LES COMPOSANTS MATÉRIELS SONT AUSSI DES MATÉRIELS
    LIBRES  
-   (08) DONT LES DISPOSITIFS NÉCESSAIRES À SA FABRICATION SONT AUSSI
    DES MATÉRIELS LIBRES  
-   (09) DONT L’UTILISATION, L’AMÉLIORATION, LA MODIFICATION, L’ÉTUDE,
    LA FABRICATION ET LA DISTRIBUTION, DE L’EXEMPLAIRE DE CETTE CHOSE
    MATÉRIELLE, DE SES FABRICATIONS COPIES, ET DES FABRICATIONS DE SES
    COPIES MODIFIÉES, SONT AUTORISÉES.  
-   (10) ET QUI, LORSQU'ELLE EST INCORPORÉE À UN DISPOSITIF, DOIT RESTER
    ACCESSIBLE, AINSI QUE SA DOCUMENTATION

~o~

TEXTE INTÉGRAL
--------------

~o~  

### **LES 10 CARACTÉRISTIQUES RÉSUMÉES D’UN MATÉRIEL LIBRE IDÉAL ABSOLU**

Ce texte apporte des informations sur la notion de matériel libre, dans
une vision idéale, absolue, canonique.  

Ce texte est écrit dans l’espoir que son contenu apporte une utilité
pour mieux comprendre cette notion de matériel libre, dans une vision
idéale, absolue, canonique du terme.  

Ce texte est aussi écrit dans l’intention de définir un étalon de la
notion de matériel libre. Pour toute chose matérielle, il devient
possible de mesurer si cette chose matérielle est éloignée de l’étalon
défini par les 10 caractéristiques de matériel libre – au sens idéal,
absolu, canonique du terme - décrites dans ce texte. Cet étalon permet
aussi de créer un point de référence dans l’optique de la rédaction de
familles de termes et conditions proposant différents niveaux de
libertés spécifiques aux choses matérielles, en référence à cet
étalon.  

#### // PRÉAMBULE IMPORTANT : BIDOUILLAGE AUTORISÉ DANS LE STRICT CADRE DE LA SPHÈRE PRIVÉE /////////////////////////////

Il est important de bien avoir à l’esprit que, chacune et chacun est
libre de bidouiller des choses matérielles dans le strict cadre de la
sphère privée (comme copier, modifier, effectuer des travaux dérivés,
étudier des objets protégés par des brevets ou des droits d’auteurs) et
dans le respect des réglementations des activités autorisées à être
réalisées dans le cadre de la sphère privée (certaines activités peuvent
être interdites par la réglementation), sans demander l’autorisation à
qui que ce soit, tant que ces bidouillages et l’utilisation de ces
bidouillages:

-   restent strictement dans le cadre de la sphère privée, et,  
-   ne sont pas faits avec une intention susceptible d’exploitation
    commerciale ou   industrielle, et,
-   ne sont pas publiés à l’extérieur de la sphère privée, bref,  
-   restent strictement dans la sphère privée pour un usage privé  

À partir du moment où, ces bidouillages ou l’utilisation de ces
bidouillages, dépassent le cadre strictement privé dans lequel ces
bidouillages ont lieu, alors, se pose toute une série de problèmes qui
restreignent la diffusion de ces bidouillages.  

Ainsi, la diffusion quelle qu’elle soit (un journal d’information, un
blog, un site internet, un wiki, une photo ou un texte publiés par un
tiers sur un vecteur de diffusion accessible au public, etc …), peut
poser des problèmes juridiques graves.  

Dès lors que ces bidouillages sont destinés à être diffusés, partagés en
dehors de la stricte sphère privée dans laquelle ont eu lieu ces
bidouillages, alors il devient nécessaire de recourir à des solutions de
diffusion particulières, afin d’éviter des problèmes juridiques.  

La notion de matériel libre, est l’une de ces solutions. Elle permet,
sous certaines conditions, de diffuser auprès du public, le savoir et la
connaissance concernant des choses matérielles fabriquées, bidouillées,
de telle façon à ce que chacune et chacun puisse utiliser ce savoir et
cette connaissance en toute liberté.  

Typiquement, dans la stricte sphère privée des fablabs, des
hackerspaces, il est tout à fait autorisé de concevoir, bidouiller,
fabriquer, extraire, transformer, assembler, modifier, utiliser, faire
fonctionner, améliorer, étudier, examiner, fabriquer des copies ou des
copies modifiées ou dérivées, d’une chose matérielle (un objet), tant
que l’on ne cherche pas à rendre l’information sur ces bidouillages
accessible au public en dehors de la sphère strictement privée dans
laquelle ces bidouillages ont eut lieu. Pour ouvrir cette connaissance
au grand public, et pour rendre libre l’utilisation qui est faite de
cette connaissance il est nécessaire, du point de vue du droit, de
passer par une publication encapsulée par des principes de matériel
libre, incluant le fait de NE PAS passer outre les obligations liées aux
notions de brevets - et équivalents.  

À cet effet, ce texte se donne pour objectif d’aider à comprendre ce que
recouvre la notion de matériel libre, dans une vision idéale, absolue,
canonique du terme.

#### // SOMMAIRE /////////////////////////////  

A. INTRODUCTION: LA NOTION DE MATÉRIEL LIBRE S’INSPIRE DES LOGICIELS
LIBRES

B. PROPOSITION DE DÉFINITION D’UN MATÉRIEL LIBRE  AU SENS IDÉAL, ABSOLU,
CANONIQUE DU TERME  

C. LA LISTE DES 10 CARACTÉRISTIQUES APPLICABLES À LA NOTION DE MATÉRIEL
LIBRE IDÉAL ABSOLU  

-   (01) C’EST UNE CHOSE MATÉRIELLE FABRIQUÉE (EXTRAITE, TRANSFORMÉE,
    ASSEMBLÉE, PRODUITE)  
-   (02) QUE L'ON PEUT FABRIQUER AVEC DES TECHNIQUES USUELLES ET
    ACCESSIBLES UNIVERSELLEMENT  
-   (03) QUI EST DÉLIVRÉE AVEC UNE DOCUMENTATION PERMETTANT DE TOUT
    SAVOIR SUR CETTE CHOSE FABRIQUÉE  
-   (04) DONT LA DOCUMENTATION DOIT ÊTRE LIBRE  
-   (05) DONT LES LOGICIELS ET LEURS DOCUMENTATIONS DOIVENT ÊTRE
    LIBRES  
-   (06) DONT LES MODIFICATIONS FONT L’OBJET D’UNE DOCUMENTATION LIBRE  
-   (07) DONT TOUS LES COMPOSANTS MATÉRIELS SONT AUSSI DES MATÉRIELS
    LIBRES  
-   (08) DONT LES DISPOSITIFS NÉCESSAIRES À SA FABRICATION SONT AUSSI
    DES MATÉRIELS LIBRES  
-   (09) DONT L’UTILISATION, L’AMÉLIORATION, LA MODIFICATION, L’ÉTUDE,
    LA FABRICATION ET LA DISTRIBUTION, DE L’EXEMPLAIRE DE CETTE CHOSE
    MATÉRIELLE, DE SES FABRICATIONS COPIES, ET DES FABRICATIONS DE SES
    COPIES MODIFIÉES, SONT AUTORISÉES.  
-   (10) ET QUI, LORSQU'ELLE EST INCORPORÉE À UN DISPOSITIF, DOIT RESTER
    ACCESSIBLE, AINSI QUE SA DOCUMENTATION  

#### // A. INTRODUCTION: LA NOTION DE MATÉRIEL LIBRE S’INSPIRE DES LOGICIELS LIBRES /////////////////////////////

La notion de matériel libre proposée ici, est construite sur l’idée de
porter les fondements des logiciels libres dans l’univers de la
fabrication de choses matérielles.  

Pour rappel, l’organisme Free Software Foundation, donne une définition
de la notion de Logiciel Libre:  

> \[// début de citation\]  
>
> «Logiciel libre» \[free software\] désigne des logiciels qui
> respectent la liberté des utilisateurs. En gros, cela veut dire que
> les utilisateurs ont la liberté d'exécuter, copier, distribuer,
> étudier, modifier et améliorer ces logiciels.»  
>
> \[// fin de citation\]  
>
> Source FSF, page URL
> &lt;https://www.gnu.org/philosophy/free-sw.fr.html&gt;  

La notion de matériel libre tente de porter les principes de cette
définition des logiciels libres, dans l’univers des fabrications de
choses matérielles.  

#### // B. PROPOSITION DE DÉFINITION D’UN MATÉRIEL LIBRE AU SENS IDÉAL, ABSOLU, CANONIQUE DU TERME /////////////////////////////

En partant de la définition des logiciels libres ci-dessus, voici une
proposition de la définition résumée du terme Matériel Libre, pris au
sens «idéal», «absolu», «canonique» du terme :  

> \[// Matériel Libre - Début de définition résumée\]  
>
> «Matériel Libre», désigne des choses matérielles fabriquées qui
> possèdent les 10 caractéristiques d’un Matériel Libre, et qui, de ce
> fait, respectent la liberté des utilisateurs, comme le font les
> logiciels libres. En gros, cela veut dire que les utilisateurs ont la
> liberté de fabriquer, d’assembler, de désassembler, de faire
> fonctionner, de réparer, de copier, de distribuer, d’étudier, de
> modifier et d’améliorer ces choses matérielles.  
>
> \[// Matériel Libre - Fin de définition résumée\]  

Toute personne qui délivre une chose matérielle possédant les 10
caractéristiques d’un Matériel Libre au sens «idéal», «absolu»,
«canonique» du terme, est encouragée à délivrer cette chose matérielle
avec une indication stipulant que c’est un matériel libre au sens
«idéal», «absolu», «canonique» du terme.  

Cette indication de matériel libre, peut être présente: sur l’objet
lui-même; ou sur une documentation livrée avec l’objet; ou au travers
d’un dispositif intermédiaire livré avec l’objet, dispositif qui indique
que c’est un matériel libre. Parfois, il est difficile de placer cette
indication sur une chose matérielle – lorsqu’elle est trop petite, trop
grande, ou lorsque c’est une matière, ou pour d’autres raisons. Dans ce
cas, la mise à disposition de cette indication autrement que sur la
chose matérielle elle-même, permet de contourner cette difficulté et
d’identifier que c’est un matériel libre.  

Parfois, une chose matérielle qui possède les 10 caractéristiques d’un
matériel libre, au sens «idéal»,«absolu», «canonique» du terme, peut
être délivrée sans aucune indication stipulant que c’est un matériel
libre. Cela peut arriver par oubli, ou pour toute autre raison. Dans ce
cas, et malgré tout, la chose matérielle ainsi que toutes les
fabrications qui en sont issues, restent tout de même des matériels
libres. Et il sera important de tout faire pour corriger ce défaut
d’indication. Les 10 caractéristiques d’un matériel libre, au sens
«idéal», «absolu», «canonique» sont listées ci-dessous.  

#### // C. LA LISTE DES 10 CARACTÉRISTIQUES APPLICABLES À LA NOTION DE MATÉRIEL LIBRE IDÉAL ABSOLU ////////////////////////////  

Le portage et l’adaptation de la notion de logiciel libre dans l’univers
de la fabrication de choses matérielles, oblige à prendre en compte, non
seulement la chose matérielle elle-même, mais aussi ses dispositifs de
fabrication, sa documentation, ses logiciels et ses composants.  

La description des 10 caractéristiques ci-dessous, couvre ces différents
aspects.  

Une chose matérielle est un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, lorsque cette chose matérielle possède bel et bien
les 10 caractéristiques suivantes.  

##### (01) UNE CHOSE MATÉRIELLE FABRIQUÉE (EXTRAITE, TRANSFORMÉE, ASSEMBLÉE, PRODUITE)  

La 1ère caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : ce doit être une chose
matérielle fabriquée.  

Ce doit être une chose artificielle composée d’éléments matériels
fabriqués, extraits, transformés, assemblés, produits. Ce doit être une
fabrication. Cette fabrication peut être, soit une création complète,
soit un assemblage de composants pré-existants, soit un mélange des
deux. Cette chose matérielle peut être : un original ; un original
modifié ; une fabrication copie de l’original ; une fabrication copie de
l’original modifié ; une fabrication copie d’une fabrication copie ; ou
une fabrication copie d’une fabrication copie modifiée.  

Si jamais cette chose est juste un plan, juste une notice de montage,
juste une notice de fonctionnement, ou si cette chose matérielle est
délivrée telle qu’elle par la nature, alors ce n’est pas une chose
matérielle fabriquée, et ce ne peut pas être un matériel libre, au sens
«idéal», «absolu», «canonique» du terme  

##### (02) QUE L'ON PEUT FABRIQUER AVEC DES TECHNIQUES USUELLES ET ACCESSIBLES UNIVERSELLEMENT  

La 2ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : la chose matérielle doit, à
l’évidence, pouvoir être fabriquée avec des techniques usuelles et
accessibles universellement, ou si ce n’est pas le cas,alors une
information disponible avec la chose matérielle, doit indiquer une
possibilité de fabriquer une alternative à cette chose matérielle en
faisant appel à des techniques usuelles et accessibles
universellement.  

Si ce n’est pas le cas, alors cette chose matérielle ne peut pas être
considérée comme un matériel libre, car les possibilités de fabriquer
une telle chose matérielle sont tellement restreintes, que dans la
pratique il est quasi impossible de fabriquer une telle chose
matérielle, et cela restreint la liberté de la fabriquer. Si vous vous
demandez ce que veut dire la notion  «usuelle et accessible
universellement», vous trouverez des informations supplémentaires dans
la note n°1 située à la fin de ce document.  

##### (03) QUI EST DÉLIVRÉE AVEC UNE DOCUMENTATION PERMETTANT DE TOUT SAVOIR SUR CETTE CHOSE FABRIQUÉE  

La 3ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : ce doit être une chose
matérielle qui est délivrée avec une documentation permettant de tout
savoir sur cette chose matérielle.  

Parfois, il est impossible de joindre cette documentation attachée
directement à la chose matérielle. Ou, parfois, la chose matérielle ne
délivre pas directement cette documentation. Dans ce cas, une indication
immédiatement visible, doit être fournie d’une façon ou d’une autre,
mentionnant l’existence de cette documentation et où il est possible d’y
avoir accès sans effort particulier.  

Cette documentation doit être facile à comprendre, de façon pédagogique.
Elle doit être composée de tout le savoir et de toute la connaissance
nécessaire:

-   d’une part pour comprendre ce matériel et son fonctionnement, et,
-   d’autre part pour le fabriquer.  

Cette documentation doit à minima proposer une alternative de
fabrication faisant appel à des techniques communément accessibles.  

Une chose matérielle qui est délivrée sans ce type de documentation,
n’est pas un matériel libre, car dans la pratique, cette absence de
documentation et de contenu de documentation, empêche de fabriquer une
telle chose matérielle et cela restreint donc la liberté de la
fabriquer.  

##### (04) DONT LA DOCUMENTATION DOIT ÊTRE LIBRE  

La 4ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : la documentation de cette chose
matérielle doit indiquer de façon fiable que les auteurs de cette
documentation autorisent explicitement à utiliser, copier, distribuer,
étudier, modifier, et améliorer librement cette documentation.  

La documentation de cette chose matérielle et le contenu de cette
documentation, doivent être dans le domaine public, ou bien ils doivent
être mis à disposition sous une licence libre adéquate, délivrant une
information incontestable qui certifie que quiconque a juridiquement
l’autorisation de l’utiliser, de la copier, de la distribuer, de
l’étudier, de la modifier, et de l’améliorer.  

Si la chose matérielle ne donne pas la preuve incontestable que sa
documentation relève du domaine public ou qu’elle est mise à disposition
sous une licence libre adéquate, alors cette chose matérielle n’est pas
un matériel libre, car il n’existe pas de certitude qu’il y ait
juridiquement l’autorisation d’utiliser, de copier, de distribuer,
d’étudier, de modifier, et d’améliorer cette documentation et son
contenu.  

Pour savoir comment une chose matérielle peut délivrer correctement une
documentation relevant du domaine public ou mise à disposition sous une
licence libre, vous pouvez lire les informations situées dans la note
n°2 à la fin de ce document.  

##### (05) DONT LES LOGICIELS ET LEURS DOCUMENTATIONS DOIVENT ÊTRE LIBRES  

La 5ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante. Ce doit être une chose
matérielle:  

-   (1) dont les éléments logiciels sont nécessairement livrés avec leur
    documentation explicative, et,
-   (2) lesquels éléments logiciels et documentation de ces logiciels,
    indiquent de façon fiable, que leurs auteurs autorisent
    explicitement l’utilisation, la copie, la distribution, l’étude, la
    modification, et l’amélioration de ces logiciels et de leurs
    documentations  

Les éléments logiciels de cette chose matérielle doivent être livrés
avec une documentation permettant de tout savoir sur ces logiciels.  

Cette documentation doit être produite pour qu’elle puisse être comprise
facilement, de façon pédagogique.  

Elle doit être composée de tout le savoir et de toute la connaissance
nécessaire, pour comprendre ce logiciel et le faire fonctionner. Les
éléments logiciels de cette chose matérielle, ainsi que la documentation
des logiciels de cette chose matérielle doivent être mis à disposition,
soit dans le domaine public, soit sous une licence libre adéquate
délivrant une information incontestable qui certifie qu’il est
juridiquement autorisé d’utiliser, de copier, de distribuer, d’étudier,
de modifier, et d’améliorer ces logiciels et leurs documentations. La
licence libre utilisée pour la documentation des logiciels de cette
chose matérielle, doit être compatible avec la licence utilisée pour ces
logiciels.  

Une chose matérielle qui ne présente pas cette caractéristique en ce qui
concerne ses logiciels et la documentation de ces logiciels, n’est pas
un matériel libre, car elle ne donne pas la certitude qu’il est
juridiquement l’autorisé d’utiliser, de copier, de distribuer,
d’étudier, de modifier, et d’améliorer correctement ces logiciels et la
documentation de ces logiciels.  

Une chose matérielle qui est délivrée avec des logiciels libres qui ne
sont pas accompagnés d’une documentation pédagogique et explicative
ayant les particularités décrites ci-dessus, n’est pas un matériel
libre, car dans la pratique, l’absence d’une documentation de ce type,
empêche de comprendre une telle chose matérielle, et cela restreint la
liberté de fabriquer cette chose matérielle.  

Pour savoir comment une chose matérielle peut délivrer correctement, un
logiciel et sa documentation, relevant du domaine public ou mis à
disposition sous une licence libre, vous pouvez consulter les
informations situées dans la note n°2 à la fin de ce document.  

##### (06) DONT LES MODIFICATIONS FONT L’OBJET D’UNE DOCUMENTATION LIBRE  

La 6ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : lorsque cette chose matérielle
est un matériel libre modifié, alors les modifications apportées,
doivent, elles aussi, être sous forme de matériel libre (au sens
«idéal», «absolu», «canonique» du terme) et faire obligatoirement
l’objet d’une documentation libre adéquate et compatible.  

Cette documentation obligatoire de matériel libre modifié doit
notamment:

-   être une documentation de matériel libre, au sens «idéal», «absolu»,
    «canonique» du terme, et,  
-   expliquer les modifications qui ont été apportées, et,  
-   être compatible avec les conditions de mises à disposition de la
    documentation de la chose matérielle publiée dans sa version
    originale non modifiée (c’est-à-dire, avant modifications).  

Une chose matérielle dont les modifications ne sont ni documentées, ni
conformes à la notion de matériel libre au sens «idéal», «absolu»,
«canonique» du terme, ne peut pas être un matériel libre au sens
«idéal», «absolu», «canonique» du terme, car dans la pratique, ce
manquement empêche techniquement et légalement de fabriquer de façon
fiable une telle chose matérielle et cela restreint donc la liberté de
fabriquer une telle chose matérielle.  

##### (07) DONT TOUS LES COMPOSANTS MATÉRIELS SONT AUSSI DES MATÉRIELS LIBRES  

La 7ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante : ce doit être une chose
matérielle dont les composants matériels doivent tous être des matériels
libres.  

La plupart du temps, une chose matérielle utilise des composants
matériels déjà existants, par assemblage, incorporation, ou par simple
utilisation / intégration à la chose matérielle. De tels composants
doivent être des matériels libres au sens «idéal», «absolu», «canonique»
du terme, pour que la chose matérielle puisse elle même être un matériel
libre, au sens «idéal», «absolu», «canonique» du terme.  

Une chose matérielle dont les composants ne seraient pas des matériels
libres, au sens «idéal», «absolu», «canonique» du terme, ne peut pas
être considérée comme un matériel libre, car les possibilités de
fabriquer les composants d’une telle chose matérielle peuvent être
tellement restreintes, que dans la pratique il n’est pas possible de les
fabriquer, et cela restreint donc la liberté de fabriquer la chose
matérielle dans son ensemble.  

##### (08) DONT LES DISPOSITIFS NÉCESSAIRES À SA FABRICATION SONT AUSSI DES MATÉRIELS LIBRES  

La 8ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante: ce doit être une chose matérielle
dont les dispositifs nécessaires à sa fabrication, doivent être des
matériels libres.  

Les machines, les procédés, les outils, les techniques, les méthodes,
qui permettent de fabriquer cette chose matérielle, doivent être des
matériels libres, au sens «idéal», «absolu», «canonique» du terme.  

Si jamais ces dispositifs de fabrication, ne sont pas des matériels
libres, au sens «idéal», «absolu», «canonique» du terme, alors la chose
matérielle issue de ces dispositifs de fabrication ne peut pas être
considérée comme un matériel libre, car la possibilité de fabriquer une
telle chose matérielle est tellement restreinte, que dans la pratique,
cela restreint la liberté de fabriquer la chose matérielle.  

##### (09) DONT L’UTILISATION, L’AMÉLIORATION, LA MODIFICATION, L’ÉTUDE, LA FABRICATION ET LA DISTRIBUTION, DE L’EXEMPLAIRE DE CETTE CHOSE MATÉRIELLE, DE SES FABRICATIONS COPIES, ET DES FABRICATIONS DE SES COPIES MODIFIÉES, SONT AUTORISÉES.  

La 9ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante: ce doit être une chose
matérielle, dont la documentation indique explicitement que  

-   (a) toute personne possédant la pleine capacité juridique sur un
    exemplaire de cette chose matérielle, est autorisée à utiliser,
    faire fonctionner, améliorer, modifier, étudier, examiner, fabriquer
    et distribuer, cet exemplaire de cette chose matérielle, et,  
-   (b) chaque personne réalisant des fabrications de copies ou des
    fabrications de copies modifiées ou dérivées de cette chose
    matérielle, est autorisée à utiliser, faire fonctionner, améliorer,
    modifier, étudier, examiner, fabriquer et distribuer, ces
    fabrications des copies ou ces fabrications de copies modifiées ou
    dérivées de cette chose matérielle qu’elle a réalisées et pour
    lesquels exemplaires elle possède la pleine capacité juridique.  

Bien entendu, toute personne (X) ayant obtenu ces autorisations, et sous
réserve d’avoir reçu la pleine capacité juridique au sujet d’un
exemplaire de cette chose matérielle, peut aussi, décider d’autoriser
une personne tierce, d’utiliser, de faire fonctionner, d’améliorer, de
modifier, d’étudier, d’examiner, de fabriquer et de distribuer,
directement cet exemplaire de cette chose matérielle pour laquelle la
personne (X) a obtenu la pleine capacité juridique.  

Toutefois, le fait que la personne (X) ne donne pas cette autorisation
pour cet exemplaire de cette chose matérielle pour lequel la personne
(X) a reçu la pleine capacité juridique, n’a pas d’influence sur le
caractère libre des autres exemplaires de cette chose matérielle.  

Particulièrement, concernant cette chose matérielle pour laquelle une
personne a reçu la pleine capacité juridique, ou concernant la
fabrication de ses copies ou la fabrication de copies modifiées ou
dérivées qu’une personne pourrait réaliser, rien ne doit interdire :  

-   d’utiliser la connaissance obtenue de leur examen, par  exemple,
    pour faire des travaux de rétro-ingénierie;  
-   d’avoir accès à leur documentation;  
-   d’avoir accès aux techniques mises en jeu;  
-   d’utiliser leurs éventuels éléments qui relèvent de brevets (ou
    équivalents), lesquels éléments brevetés (ou équivalents) devant
    alors être obligatoirement mis à disposition sous la forme de
    matériel libre au sens «idéal», «absolu»,  «canonique» du terme,
    incluant une autorisation irrévocable, mondiale, illimitée,
    d’utilisation exempte de redevance (gratuité complète des
    redevances).  

Une chose matérielle qui ne donne pas ces autorisations, n’est pas un 
matériel libre au sens «idéal», «absolu», «canonique» du terme, car  les
possibilités de fabriquer une telle chose matérielle sont tellement
restreintes, que dans la pratique il n’est pas possible de fabriquer une
telle chose matérielle, et cela restreint la liberté de la fabriquer.  

##### (10) ET QUI, LORSQU'ELLE EST INCORPORÉE À UN DISPOSITIF, DOIT RESTER ACCESSIBLE, AINSI QUE SA DOCUMENTATION  

La 10ème caractéristique d’un matériel libre, au sens «idéal», «absolu»,
«canonique» du terme, est la suivante: ce doit être une chose matérielle
qui doit rester accessible, ainsi que sa documentation, y compris
lorsqu’elle est incorporée à quelque chose d’autre.  

Il peut arriver que cette chose matérielle soit utilisée en tant que
composante d’un ensemble. Lorsque cela arrive, alors la façon dont cet
ensemble est constitué doit permettre l’accès physique à cette chose
matérielle sans effort particulier en utilisant des techniques usuelles
et accessibles universellement. Pour comprendre comment avoir accès à
cette chose matérielle, et comment la retirer ou la replacer, une notice
explicative, accessible sans effort particulier, doit être fournie avec
cet ensemble.  

  

~o~ FIN DE LA LISTE DES 10 CARACTÉRISTIQUES APPLICABLES À LA NOTION DE
MATÉRIEL LIBRE IDÉAL ABSOLU ~o~

  

#### // NOTES ET INFORMATIONS ADDITIONNELLES ///////////

##### NOTE 1:  EXPLICATIONS CONCERNANT LES TECHNIQUES USUELLES ET ACCESSIBLES UNIVERSELLEMENT  

Grosso-modo, une technique est devenue usuelle et accessible
universellement, :    

-   1) dès lors qu'il est communément admis que cette technique existe
    et qu’elle fait l’objet d’un savoir commun dans la plupart des
    régions du monde, et,  
-   2) dès lors qu'elle est accessible au plus grand nombre sans trop
    d’effort particulier dans le contexte temporel et régional où l’on
    se trouve.  

Bien entendu, les techniques usuelles et accessibles universellement
évoluent avec le temps, en fonction de l’évolution de l’humanité.
Déterminer si une technique est usuelle et accessible universellement,
ou si elle ne l'est pas, dépend du contexte et de l’époque dans lesquels
on se trouve. Une technique qui n’était ni usuelle ni accessible
universellement à un moment donné dans une région du monde, peut le
devenir quelque temps plus tard. Le contraire peut aussi arriver.  

##### NOTE 2:  EXPLICATIONS CONCERNANT LA FAÇON DE VÉRIFIER QUE QUELQUE CHOSE EST DANS LE DOMAINE PUBLIC OU MIS À DISPOSITION SOUS LICENCE LIBRE  

Grosso modo, vérifier que quelque chose est dans le domaine public ou
publié sous une licence libre, signifie faire au moins deux choses
essentielles:  

-   1) il faut détecter que cette chose est accompagnée d’une indication
    (une information) qui stipule que cette chose est dans le domaine
    public ou publiée sous une licence libre, et,  
-   2) il faut aussi détecter que cette indication est complètement
    fiable et incontestable.  

Lorsqu’une chose est, incontestablement entrée dans le domaine public,
ou qu’elle est incontestablement mise à disposition sous une licence
libre adéquate, alors cela délivre une information incontestable 
certifiant juridiquement, la liberté d’utiliser, de copier, de
distribuer, d’étudier, de modifier, et d’améliorer cette chose, dans les
conditions imposées par la législation en vigueur applicable concernant
cette chose et ses utilisations.  

Une telle chose peut-être: une documentation (pédagogique, éducative,
technique, etc ...); un logiciel; la documentation d’un logiciel; une
forme esthétique; ainsi que toutes les choses couvertes par le droit
applicable.  

Pour vérifier incontestablement que quelque chose est entrée dans le
domaine public ou est mise à disposition sous une licence libre
adéquate, voici ce qu’il est possible d’essayer de faire.  

Tout d’abord, il est possible d’essayer de rechercher si cette chose
inclus une mention explicite stipulant qu’elle est entrée dans le
domaine public, ou qu’elle est mise à disposition sous une licence
libre. Cette mention est souvent visible immédiatement. La trouver ne
devrait pas demander un effort particulier. Si cette mention est
difficile à trouver, il est recommandé de le signaler pour que cela
change. En fonction des possibilités, cette mention est parfois:  

-   un texte très court comme «relevant du domaine public», ou «mise à
    disposition sous licence ...» ou,  
-   un simple logo explicite, ou,
-   une association d’un texte et d’un logo explicite.  

La présence d’une telle mention, d’un tel logo, d’une telle indication,
doit indiquer l’identification précise des termes et conditions
juridiques qui s’appliquent: le domaine public; ou une le nom explicite
de la licence libre applicable.

Hélas, actuellement, il n’existe pas de logo pour signifier « entré dans
le domaine public ». C’est un problème. Car cela signifie qu’il n’est
pas possible de repérer par un simple pictogramme que la chose est dans
le domaine public. L’existence d’un tel pictogramme reconnu, serait bien
utile. Ce serait quelque chose à faire.  

À l’inverse, en ce qui concerne la mise à disposition sous licence
libre, il existe de nombreux logos. En effet, de nombreuses licences
libres ont développé leurs logos, reliant l’utilisation de chacun de
leur logo avec les termes de chacune des licences à laquelle le logo
fait référence. Par exemple, la présence du logo de la licence Art
Libre, signifie que la chose est publiée sous licence Art Libre.  

Bien sûr, tout le monde ne sait pas si telle ou telle licence est une
licence libre, si tel logo est un logo de licence libre.  

Heureusement, il existe des moyens pour savoir si une licence, un logo,
fait référence à une licence libre.  

Par exemple, il existe un excellent moyen pour savoir si la licence
indiquée est une licence libre, et pour savoir si elle est adéquate. Ce
moyen consiste à vérifier que la Free Software Foundation (FSF) classe
belle et bien cette licence parmi les licences libres utilisables, en
allant consulter le site de la Free Software Foundation à la page
suivante:

-   &lt;https://www.gnu.org/licenses/license-list.fr.html&gt;.  

Si la licence mentionnée avec la chose mise à disposition, fait partie
de cette liste de licences libres publiée par la FSF, alors la licence
mentionnée est une licence libre. Sinon, elle ne l’est pas. C’est aussi
simple que ça.  

Dans certains cas, il peut arriver qu’il soit difficile, ou impossible,
d’avoir la certitude d’avoir détecté incontestablement de telles
mentions de domaine public ou de licence libre. Quand cela arrive, alors
cela signifie qu’il n’y a aucune certitude que la chose est entrée dans
le domaine public ou est mise à disposition sous licence libre.  

Par ailleurs, il est important de bien avoir à l’esprit, que le simple
fait d’être en présence d’indications de domaine public ou de licence
libre, n’est pas suffisant pour permettre d’avoir la certitude que la
chose est belle et bien dans le domaine public ou mise à disposition
sous licence libre.  

En effet, rien n’indique que les personnes qui apposent cette indication
de domaine public ou de licence libre, le font correctement, sans
intention de supercherie, et si elles ont la capacité juridique de le
faire.  

Pour obtenir une totale certitude, il faut obtenir une information à
caractère incontestable qui démontre que:  

-   les auteurs indiqués sont bels et bien les auteurs, et,  
-   les auteurs indiqués ont bels et bien procédé à tout ce qu’il
    fallait faire pour publier correctement la chose et pour donner
    incontestablement l’autorisation de l’utiliser, de la copier, de la
    distribuer, de l’étudier, de la modifier, et de l’améliorer, dans
    les conditions imposées par la loi en vigueur concernant cette chose
    et ses utilisations, et
-   les auteurs possèdent la capacité juridique de le faire  

Si pour une personne, il n’est pas possible, par elle même, d’arriver à
obtenir cette certitude, il lui est possible d’essayer de vérifier si
cette chose est publiée au travers de dispositifs de confiance.

En effet, certains vecteurs de diffusion sont très exigeants sur leurs
publications, et procèdent à ces vérifications préalables. En consultant
ces vecteurs de diffusion de confiance, cela permet d’accroître
considérablement le niveau de certitude qu’il est possible d’avoir dans
la conformité des conditions de publication de la chose mise à
disposition dans le domaine public ou sous licence libre.  

Pour qu’un vecteur de diffusion soit pleinement de confiance, il est
préférable que ce vecteur soit aussi reconnu officiellement par les
tribunaux comme source officielle de publication pour les choses
relevant de la réglementation applicable aux droits des auteurs.  

Il est aussi possible d’interroger des personnes de confiance et
compétentes pour avoir leur avis. Par exemple, la FSF ou tout autre
organisme ou personne individuelle réputée pour répondre correctement à
ce type de question, sont des entités qu’il est possible d’interroger.  

Lorsque vous n’obtenez pas ces certitudes, il existe une probabilité
pour que les indications « domaine public » ou « mise à disposition sous
licence libre », ne soient pas recevables par les juges sollicités pour
trancher sur ces questions en cas de conflit juridique.  

  

~o~ FIN DU TEXTE INTÉGRAL LES 10 CARACTÉRISTIQUES D’UN  MATÉRIEL LIBRE
AU SENS IDÉAL ABSOLU CANONIQUE DU TERME ~o~  

  

/// Note de clôture de document ////:  
---------------------------------------

§ - URL du texte original  
---------------------------

Ce document est une mise en page du texte original. Le texte original
est développé et publié sur Framagit (une instance gitlab CE proposée
par l’association Framasoft) à l’adresse:  

-   <https://framagit.org/materiel-libre/materiel-libre>  

Vous pouvez participer à l’amélioration de ce texte, au travers de cette
instance Framagit - prenez un compte chez Framagit, allez avec un
navigateur web à l'URL ci-dessus, et demandez à faire partie du
projet.  

§ - Termes et Conditions de publication de ce document et de son contenu
------------------------------------------------------------------------

Ce document et son contenu, sont une œuvre de l'esprit.

### Auteurs de cette œuvre de l'esprit

Le Laboratoire International Incongru Iconoclaste Imaginaire Improbable
Inexistant de Recherche Expérimentale AppLiquée sur le Matériel Libre
(LAB-6I-REAL-MAT-LIB), et L’association « La Labomedia », et d’autres
personnes de part le monde, incluant: Maxime, Gaziel, b01, Gilou, ... ,
Antoine\_C et bien d'autres.  

Pour contacter les auteurs, prenez un compte chez Framagit, allez avec
un navigateur web à l'URL indiquée ci-dessus, et demandez à faire partie
du projet. Vous pourrez alors entrer en contact avec les auteurs.  

### Notice de termes et conditions de mise à disposition de cette œuvre de l'esprit

[![Licence Creative
Commons](https://i.creativecommons.org/l/by-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nd/4.0/)  
<span dct="http://purl.org/dc/terms/" property="dct:title">Cette œuvre
de l'esprit intitulée "LES 10 CARACTÉRISTIQUES D’UN MATÉRIEL LIBRE AU
SENS IDÉAL ABSOLU CANONIQUE DU TERME</span>", dont les auteurs sont
indiqués ci-dessus, est:

-   mise à disposition selon les termes de la [licence Creative Commons
    Attribution - Pas de Modification 4.0
    International](http://creativecommons.org/licenses/by-nd/4.0/)
    &lt;http://creativecommons.org/licenses/by-nd/4.0/&gt;   
-   disponible à l'URL:
    <https://framagit.org/materiel-libre/materiel-libre>  
-   fondé(e) sur une œuvre originale disponible à l'URL:
    <https://framagit.org/materiel-libre/materiel-libre>   

### Résumé de cette licence  

Ceci est un résumé (et non pas un substitut) de la licence Creative
Commons Attribution - Pas de Modification 4.0 International (CC BY-ND
4.0)
&lt;<https://creativecommons.org/licenses/by-nd/4.0/legalcode.fr>&gt;.  

Avertissement: Ce résumé n'indique que certaines des dispositions clé de
la licence. Ce n'est pas une licence, il n'a pas de valeur juridique.
Vous devez lire attentivement tous les termes et conditions de la
licence Creative Commons Attribution - Pas de Modification 4.0
International (CC BY-ND 4.0)
&lt;<https://creativecommons.org/licenses/by-nd/4.0/legalcode.fr>&gt;
avant d'utiliser le matériel licencié. Creative Commons n'est pas un
cabinet d'avocat et n'est pas un service de conseil juridique.
Distribuer, afficher et faire un lien vers le résumé ou la licence ne
constitue pas une relation client-avocat ou tout autre type de relation
entre vous et Creative Commons.  
.  
*Vous êtes autorisé à :*  

-   Partager — copier, distribuer et communiquer le matériel par tous
    moyens et sous tous formats pour toute utilisation, y compris
    commerciale.
-   L'Offrant ne peut retirer les autorisations concédées par la licence
    tant que vous appliquez les termes de cette licence.  

*Selon les conditions suivantes :*

-   Attribution — Vous devez créditer l'Œuvre, intégrer un lien vers la
    licence et indiquer si des modifications ont été effectuées à
    l'Oeuvre. Vous devez indiquer ces informations par tous les moyens
    raisonnables, sans toutefois suggérer que l'Offrant vous soutient ou
    soutient la façon dont vous avez utilisé son Oeuvre.
-   Pas de modifications — Dans le cas où vous effectuez un remix, que
    vous transformez, ou créez à partir du matériel composant l'Oeuvre
    originale, vous n'êtes pas autorisé à distribuer ou mettre à
    disposition l'Oeuvre modifiée.
-   Pas de restrictions complémentaires — Vous n'êtes pas autorisé à
    appliquer des conditions légales ou des mesures techniques qui
    restreindraient légalement autrui à utiliser l'Oeuvre dans les
    conditions décrites par la licence.  

*Notes:*  

-   Vous n'êtes pas dans l'obligation de respecter la licence pour les
    éléments ou matériel appartenant au domaine public ou dans le cas où
    l'utilisation que vous souhaitez faire est couverte par une
    exception.
-   Aucune garantie n'est donnée. Il se peut que la licence ne vous
    donne pas toutes les permissions nécessaires pour votre utilisation.
    Par exemple, certains droits comme les droits moraux, le droit des
    données personnelles et le droit à l'image sont susceptibles de
    limiter votre utilisation.  

Pour accéder à une copie de cette licence, merci d’accéder à :  

-   &lt;<https://creativecommons.org/licenses/by-nd/4.0/legalcode.fr>&gt;  

### § - Pourquoi le choix de cette licence pour ce texte  ?

Pour comprendre pourquoi cette licence est choisie pour publier ce
texte, merci de consulter :  

-   &lt;<https://www.gnu.org/licenses/license-list.fr.html#OpinionLicenses>&gt;

  

~~~~~~ ooo FIN DE DOCUMENT ooo ~~~~~~~  

